import sys
import random

wrongcall : str = "Unrecognized command format"
helpstring : str = "Correct call:\n\tpython secure_password_generator.py [num_characters] [-x [string]] [-i [string]]\
               \n\n\t-x\texclude characters\n\t-i\tinclude characters\n\t-a\tonly include alphanumeric characters\n\t-h\tdisplay this help"

def main(args : list):
    # define excluded characters
    excluded = ["^", "`"]
    included = [chr(i) for i in range(33, 127)]
    default_len : int = 12

    if len(args) == 0:
        pass
    else:
        skip = 0
        nflag = 0
        for i, arg in enumerate(args):
            if skip:
                skip = 0
                continue
            elif arg == "-h":            
                print(helpstring)
                sys.exit(0)
            elif arg == "-x":
                if len(args) <= i+1:
                    print(wrongcall)
                    print(helpstring)
                    sys.exit(1)
                else:
                    for s in args[i+1]:
                        if s not in excluded:
                            excluded.append(s)
                    skip = 1
            elif arg == "-i":
                if len(args) <= i+1:
                    print(wrongcall)
                    print(helpstring)
                    sys.exit(1)
                else:
                    for s in args[i+1]:
                        if s not in included:
                            included.append(s)
                    skip = 1
            elif arg == "-a":
                included = [chr(x) for x in range(48, 58)] + [chr(x) for x in range(65, 91)] + [chr(x) for x in range(97, 123)]
                excluded = [c for c in [chr(x) for x in range(33, 127)] if c not in included]
            elif not nflag:        
                default_len = int(arg)
                nflag = 1
            else:
                print(wrongcall)
                print(helpstring)
                sys.exit(1)
    print("The following characters will be included: ", end="")
    for f in included[:-1]:
        print(f, end="")
    print(included[-1])
    print("The following characters will be excluded: ", end="")
    for f in excluded[:-1]:
        print(f, end="")
    print(excluded[-1])
    print(f"Generating random password of size {default_len}")
    while default_len:
        rnd = included[random.randint(0, len(included))]
        if rnd not in excluded:
            print(rnd, end="")
            default_len -= 1
    print()

if __name__ == "__main__":
    main(sys.argv[1:])